package org.orangeplayer.consumatore.consumer;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;

import org.orangeplayer.common.response.PlayerResponse;
import org.orangeplayer.common.response.Song;
import org.orangeplayer.common.sys.SysInfo;
import org.orangeplayer.consumatore.net.URLConnector;

import java.io.BufferedReader;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;

public abstract class Consumer {
    protected Gson gson;
    protected URLConnector connection;

    protected final String servicePath;

    /*public Consumer() {
        this("localhost");
    }

    public Consumer(String host) {
        //this(host, new ConnectionInfo().getNumericProperty(PORT));
        this(host, null);
    }*/

    public Consumer(String host, String servicePath) {
        this(host, SysInfo.SERVER_PORT, servicePath);
    }

    /*public Consumer(String host, int port) {
        this(host, port, null);
    }*/

    public Consumer(String host, int port, String servicePath) {
        connection = new URLConnector(host, port);
        gson = new Gson();
        this.servicePath = servicePath;
    }

    protected String formatPath(String urlPath) {
        return servicePath.concat(urlPath);
    }

    /*protected <T> Type getTypeOf(Class<T>... clazz) {
        return new TypeToken<T>(){}.getType();
    }*/

    protected Object consume(String urlPath, Type type, String... parameters) throws IOException {
        return gson.fromJson(connection.openUrlReader(formatPath(urlPath), parameters), type);
    }

    protected Number consumeNumber(String urlPath, String... parameters) throws IOException {
        String str = consumeString(urlPath, parameters);
        return str.contains(".") ? Double.parseDouble(str) : Long.parseLong(str);
    }

    protected boolean consumeBoolean(String urlPath, String... parameters) throws IOException {
        return Boolean.parseBoolean(consumeString(urlPath, parameters));
    }

    protected void consumeNothing(String urlPath, String... parameters) throws IOException {
        connection.openUrl(formatPath(urlPath), parameters);
    }

    protected String consumeString(String urlPath, String...parameters) throws IOException {
        urlPath = formatPath(urlPath);
        String urlContent = connection.getUrlContent(urlPath, parameters);
        return urlContent;
    }

    protected byte[] consumeByteArray(String urlPath, String...parameters) throws IOException {
        urlPath = formatPath(urlPath);
        return connection.getUrlData(urlPath, parameters);
    }

    protected PlayerResponse consumeResponse(String urlPath, String...parameters) throws IOException {
        return (PlayerResponse) consume(urlPath, new TypeToken<PlayerResponse>(){}.getType(), parameters);
    }

    protected JsonElement consumeJson(String urlPath, String...parameters) throws IOException {
        return new JsonParser().parse(consumeString(urlPath, parameters));
    }

    protected Song consumeSong(String urlPath, String...parameters) throws IOException {
        urlPath = formatPath(urlPath);
        PlayerResponse<Song> response = gson.fromJson(connection.openUrlReader(urlPath, parameters),
                new TypeToken<PlayerResponse<Song>>(){}.getType());
        return response.getResponse();
    }

    protected Object[] consumeArray(String urlPath, Type type, String...parameters) throws IOException {
        return gson.fromJson(connection.openUrlReader(formatPath(urlPath), parameters), type);
    }

    protected ArrayList<Song> consumeListSongs(String urlPath, String...parameters) throws IOException {
        return (ArrayList<Song>) consumeResponse(urlPath, parameters).getResponse();
    }

    protected ArrayList consumeList(String urlPath, Type type, String...parameters) throws IOException {
        return gson.fromJson(connection.openUrlReader(formatPath(urlPath), parameters), type);
    }

}
