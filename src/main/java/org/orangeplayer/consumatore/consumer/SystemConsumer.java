package org.orangeplayer.consumatore.consumer;

import com.google.gson.reflect.TypeToken;
import org.orangeplayer.common.beans.DeviceInfo;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class SystemConsumer extends Consumer {
    public SystemConsumer(String host) {
        super(host, "/sys");
    }

    public SystemConsumer(String host, int port, String servicePath) {
        super(host, port, servicePath);
    }

    public boolean isMounted(String devFileName) throws IOException {
        return consumeBoolean("/ismounted", devFileName);
    }

    public boolean mountDevice(String devFileName) throws IOException {
        return consumeBoolean("/mount", devFileName);
    }

    public boolean umountDevice(String devFileName) throws IOException {
        return consumeBoolean("/umount", devFileName);
    }

    public ArrayList<DeviceInfo> getListDevices() throws IOException {
        return consumeList("/getdevs", new TypeToken<ArrayList<DeviceInfo>>(){}.getType());
    }

    public ArrayList<DeviceInfo> getMountedDevices() throws IOException {
        return consumeList("/getmounted", new TypeToken<ArrayList<DeviceInfo>>(){}.getType());

    }

    public ArrayList<DeviceInfo> getDevicesLabel() throws IOException {
        return consumeList("/getlabels", new TypeToken<ArrayList<DeviceInfo>>(){}.getType());
    }

    public String getLabel(String devFile) throws IOException {
        return consumeString("/getlabel/", devFile);
    }

    public String getMountPoint(String devFile) throws IOException {
        return consumeString("/getpath/", devFile);
    }

    // probar
    public File[] getFiles(String folder) throws IOException {
        return gson.fromJson(consumeString("/listfiles/", folder), new TypeToken<File[]>(){}.getType());
    }

}
