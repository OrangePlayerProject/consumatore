package org.orangeplayer.consumatore.consumer;

import org.orangeplayer.common.response.PlayerResponse;
import org.orangeplayer.common.response.Song;

import java.io.IOException;
import java.util.ArrayList;

public class MusicConsumer extends Consumer {
    public MusicConsumer(String host) {
        super(host, "/player/music");
    }

    public MusicConsumer(String host, int port, String servicePath) {
        super(host, port, servicePath);
    }

    public boolean isPlayerStarted() throws Exception {
        return Boolean.valueOf(consumeString("/isstarted"));
    }

    public boolean isPlayerPaused() throws Exception {
        return Boolean.valueOf(consumeString("/ispaused"));
    }

    public boolean isPlayerStopped() throws Exception {
        return Boolean.valueOf(consumeString("/isstopped"));
    }

    public boolean isPlayerPlaying() throws Exception {
        return Boolean.valueOf(consumeString("/isplaying"));
    }

    public boolean startPlayer() throws IOException {
        return consumeBoolean("/start");
    }

    public PlayerResponse<Boolean> play(String... path) throws IOException {
        return consumeResponse("/player", path);
    }

    public void stop() throws IOException {
        consumeNothing("/stop");
    }

    public void resume() throws IOException {
        consumeNothing("/resume");
    }

    public void pause() throws IOException {
        consumeNothing("/pause");
    }

    public PlayerResponse<String> next() throws IOException {
        return consumeResponse("/next");
    }

    public PlayerResponse<String> prev() throws IOException {
        return consumeResponse("/prev");
    }

    public PlayerResponse<String> mute() throws IOException {
        return consumeResponse("/mute");
    }

    public PlayerResponse<String> unmute() throws IOException {
        return consumeResponse("/unmute");
    }

    public float getGain() throws IOException {
        return Float.parseFloat(consumeString("/getgain"));
    }

    public PlayerResponse<String> setGain(float volume) throws IOException {
        return consumeResponse("/setgain", String.valueOf(Math.round(volume)));
    }

    public PlayerResponse<String> shutdown() throws IOException {
        return consumeResponse("/shutdown");
    }

    public PlayerResponse<String> seek(int secs) throws IOException {
        return consumeResponse("/seek", String.valueOf(secs));
    }

    public PlayerResponse<String> seekFld(int jumps, boolean isNext) throws IOException {
        return consumeResponse("/seekfld", String.valueOf(jumps), String.valueOf(isNext));
    }

    public PlayerResponse<String> gotoSecond(int sec) throws IOException {
        return consumeResponse("/goto", String.valueOf(sec));
    }

    public byte[] getCoverFromCurrent() throws IOException {
        return consumeByteArray("/currentcover");
    }

    public byte[] getCover(String path) throws IOException {
        return consumeByteArray("/cover", path);
    }

    public Song getCurrentInfo() throws IOException {
        //Log.d("martin", "Before getCurrent");
        return consumeSong("/current");
    }

    public ArrayList<Song> getListSongs() throws IOException {
        return consumeListSongs("/list");
    }

    public ArrayList<Song> getFullListSongs() throws IOException {
        return consumeListSongs("/listfull");
    }

    public String getProgress() throws IOException {
        return consumeString("/progress");
    }

    /*public static void main(String[] args) throws IOException {
        URLConnector connection = new URLConnector("192.168.56.1", SysInfo.SERVER_PORT);
        BufferedReader bufferedReader = connection.openUrlReader("/player/music/setgain", "100");
        Gson gson = new Gson();
        Type type = new TypeToken<PlayerResponse>(){}.getType();
        PlayerResponse<String> response = gson.fromJson(bufferedReader, type);
        System.out.println(response.getCode());
        System.out.println(response.getResponse());
    }*/

}
