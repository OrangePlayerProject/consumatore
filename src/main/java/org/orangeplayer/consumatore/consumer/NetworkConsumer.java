package org.orangeplayer.consumatore.consumer;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.orangeplayer.common.beans.NetworkInfo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class NetworkConsumer extends Consumer {
    public NetworkConsumer(String host) {
        super(host, "/net");
    }

    public NetworkConsumer(String host, String servicePath) {
        super(host, servicePath);
    }

    public NetworkConsumer(String host, int port, String servicePath) {
        super(host, port, servicePath);
    }

    public boolean connect() throws IOException {
        return Boolean.valueOf(consumeString("/link"));
    }

    public boolean disconnect() throws IOException {
        return Boolean.valueOf(consumeString("/unlink"));
    }

    public boolean isConnected() throws IOException {
        return Boolean.valueOf(consumeString("/islinked"));
    }

    public boolean connectWifi(String ssid, String passw) throws IOException {
        return Boolean.valueOf(consumeString("/connect/", ssid, passw));
    }

    public String getEthernetIp() throws IOException {
        return consumeString("/ethip").trim();
    }

    public String getWlanIp() throws IOException {
        return consumeString("/wlanip").trim();
    }

    public ArrayList<NetworkInfo> getNetworks() throws IOException {
        return consumeList("/getnetworks", new TypeToken<ArrayList<NetworkInfo>>(){}.getType());
    }



}
