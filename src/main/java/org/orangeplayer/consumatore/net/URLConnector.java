package org.orangeplayer.consumatore.net;

import android.util.Log;
import android.widget.Toast;

import org.bytebuffer.ByteBuffer;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

import static org.orangeplayer.common.util.SysUtil.formatParametersAsString;

public class URLConnector {
    private final String host;
    private final int port;

    public URLConnector(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public URL getUrl(String path, String... parameters) throws MalformedURLException {
        URL url;
        if (!path.endsWith("/"))
            path = path.concat("/");

        if (parameters == null || parameters.length == 0)
            url = new URL("http", host, port, path);
        else {
            StringBuilder sbFullPath =
                    new StringBuilder(path).append(formatParametersAsString(parameters));
            url = new URL("http", host, port, sbFullPath.toString());
        }
        return url;
    }

    public void openUrl(String path, String... parameters) {
        try {
            getUrl(path, parameters).getContent();
        } catch (IOException e) {}
    }

    public URLConnection openUrlConnection(String path, String... parameters) throws IOException {
        return getUrl(path, parameters).openConnection();
    }

    public InputStream openUrlStream(String path, String... parameters) {
        URL url = null;
        try {
            url = getUrl(path, parameters);
            return url.openStream();
        } catch (FileNotFoundException e) {
            Logger.getLogger("URLConnector").log(Level.SEVERE, "Error 404: "+url.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public BufferedReader openUrlReader(String path, String... parameters) throws IOException {
        return new BufferedReader(new InputStreamReader(openUrlStream(path, parameters)));
    }

    public byte[] getUrlData(String path, String... parameters) throws IOException {
        InputStream inputStream = openUrlStream(path, parameters);
        ByteBuffer byteBuffer = new ByteBuffer(10240);

        int read;
        byte[] buffer = new byte[1024];
        while ((read = inputStream.read(buffer)) != -1)
            byteBuffer.addFrom(buffer, 0, read);
        return byteBuffer.drain();
    }

    public String getUrlContent(String path, String... parameters) throws IOException {
        StringBuilder sbContent = new StringBuilder();
        BufferedReader reader = openUrlReader(path, parameters);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N)
            reader.lines().forEach(sbContent::append);
        else {
            String line;
            while ((line = reader.readLine()) != null)
                sbContent.append(line);
        }
        return sbContent.toString();
    }

}
