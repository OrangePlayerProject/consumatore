package org.orangeplayer.consumatore.net;

import android.os.Build;

import org.orangeplayer.common.sys.SysInfo;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ServizioFinder extends Thread {
    private final String networkAddress;
    private ArrayList<String> listAddresses;
    private LinkedList<Future<String>> listFutures;

    private volatile String servizioHost;

    public ServizioFinder(String networkAddress) {
        this.networkAddress = networkAddress;
        listAddresses = new ArrayList<>();
        listFutures = new LinkedList<>();
        loadAddresses();
        setName("ServizioFinder "+getId());
    }

    private void loadAddresses() {
        StringBuilder sbIp = new StringBuilder();
        for (int i = 1; i < 255; i++)
            listAddresses.add(sbIp.delete(0, sbIp.length())
                    .append(networkAddress).append('.').append(i).toString());
    }

    private void scanServizioHost() {
        servizioHost = null;
        ExecutorService executor = Executors.newFixedThreadPool(254);

        for (int i = 0; i < listAddresses.size(); i++)
            listFutures.add(executor.submit(new ThreadScanner(listAddresses.get(i), SysInfo.SERVER_PORT)));

        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Future<String> futureFound = listFutures.parallelStream()
                    .filter(future -> {
                        try {
                            while (!future.isDone()) {
                            }
                            return !future.get().isEmpty();
                        } catch (ExecutionException | InterruptedException e) {
                            return false;
                        }
                    }).findFirst().orElse(null);
            try {
                if (futureFound != null)
                    servizioHost = futureFound.get();
            } catch (ExecutionException | InterruptedException e) {}
        }*/
        String ip;
        for (Future<String> future : listFutures) {
            while (!future.isDone()) {
            }
            try {
                ip = future.get();
                if (!ip.isEmpty()) {
                    servizioHost = ip;
                    break;
                }
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
        executor.shutdownNow();
    }

    public synchronized String getServizioHost() {
        while (isAlive());
        return servizioHost;
    }

    @Override
    public void run() {
        scanServizioHost();
    }
}
