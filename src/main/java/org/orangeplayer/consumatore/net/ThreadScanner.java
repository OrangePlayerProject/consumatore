package org.orangeplayer.consumatore.net;

import org.orangeplayer.common.sys.SysInfo;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.util.concurrent.Callable;

public class ThreadScanner implements Callable<String> {
    private Socket sockScanner;
    private String host;
    private int port;

    public ThreadScanner(String host, int port) {
        this.host = host;
        this.port = port;
    }

    private boolean hasConnection() {
        try {
            sockScanner = new Socket(host, port);
            if (sockScanner.isBound() && sockScanner.isConnected()) {
                OutputStream out = sockScanner.getOutputStream();
                out.write("testing".getBytes());
                out.flush();
                out.close();
                sockScanner.close();
                return true;
            }
            else
                return false;
        } catch (IOException e) {
            return false;
        }
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    @Override
    public String call() throws Exception {
        return hasConnection() ? host : "";
    }

}
