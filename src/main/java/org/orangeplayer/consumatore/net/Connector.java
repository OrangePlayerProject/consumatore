package org.orangeplayer.consumatore.net;

import org.orangeplayer.common.sys.SysInfo;
import org.orangeplayer.consumatore.consumer.MusicConsumer;
import org.orangeplayer.consumatore.consumer.NetworkConsumer;
import org.orangeplayer.consumatore.consumer.SystemConsumer;

public class Connector {

    private final MusicConsumer musicConsumer;
    private final NetworkConsumer networkConsumer;
    private final SystemConsumer systemConsumer;

    public Connector() {
        this("localhost");
    }

    public Connector(String host) {
        //this(host, new ConnectionInfo().getNumericProperty(PORT));
        this(host, SysInfo.SERVER_PORT);
    }

    public Connector(String host, int port) {
        musicConsumer = new MusicConsumer(host, port, "/player/music");
        networkConsumer = new NetworkConsumer(host, port, "/net");
        systemConsumer = new SystemConsumer(host, port, "/sys");
    }

    public MusicConsumer getMusicConsumer() {
        return musicConsumer;
    }

    public NetworkConsumer getNetworkConsumer() {
        return networkConsumer;
    }

    public SystemConsumer getSystemConsumer() {
        return systemConsumer;
    }
}
