package org.orangeplayer.consumatore.cli;/*package org.orange.org.orangeplayer.consumatore.cli;

import com.google.gson.Gson;
import Connector;
import OrangeConnector;

import java.io.IOException;
import java.util.Scanner;

import static org.orange.common.cmd.PlayerOrder.*;
import static org.orange.common.cmd.PlayerOrder.GETCOVER;
import static org.orange.common.cmd.PlayerOrder.GETINFO;

public class Main {
    public static void main(String[] args) throws IOException, InterruptedException {
        OrangeConnector connector = new Connector("localhost");
        Gson gson = ((Connector) connector).getGson();
        //ArrayList<Song> listSong = connector.getListSongs();

        Scanner scan = new Scanner(System.in);
        String line = null;
        String order;
        String[] split;

        while (true) {
            line = scan.nextLine().trim();
            if (line != null) {
                order = line.contains(" ") ? line.split(" ")[0] : line;
                split = line.split(" ");
                try {
                    switch (order) {
                        case START:
                            System.out.println(gson.toJson(connector.startPlayer()));
                            break;

                        case PLAY:
                            System.out.println(gson.toJson(connector.resume()));
                            break;

                        case STOP:
                            System.out.println(gson.toJson(connector.stop()));
                            break;

                        case RESUME:
                            System.out.println(gson.toJson(connector.resume()));
                            break;

                        case PAUSE:
                            System.out.println(gson.toJson(connector.pause()));
                            break;

                        case NEXT:
                            System.out.println(gson.toJson(connector.next()));
                            break;

                        case PREV:
                            System.out.println(gson.toJson(connector.prev()));
                            break;

                        case MUTE:
                            System.out.println(gson.toJson(connector.mute()));
                            break;

                        case UNMUTE:
                            System.out.println(gson.toJson(connector.unmute()));
                            break;

                        case SETGAIN:
                            System.out.println(gson.toJson(connector.setGain(Float.parseFloat(split[1]))));
                            break;

                        case SHUTDOWN:
                            System.out.println(gson.toJson(connector.shutdown()));
                            break;

                        case SEEK:
                            System.out.println(gson.toJson(connector.seek(Integer.parseInt(split[1]))));
                            break;

                        case SEEKFLD:
                            System.out.println(gson.toJson(connector.seekFld(Integer.parseInt(split[1]),
                                    Boolean.parseBoolean(split[2]))));
                            break;

                        case GOTOSEC:
                            System.out.println(gson.toJson(connector.gotoSecond(Integer.parseInt(split[1]))));
                            break;

                        case GETCOVER:
                            System.out.println(gson.toJson(connector.getCoverFromCurrent()));
                            break;

                        case GETINFO:
                            System.out.println(gson.toJson(connector.getCurrentInfo()));
                            break;
                        case DISCONNECT:
                            System.out.println(gson.toJson(connector.disconnect()));
                            break;
                    }
                    if (order.equals(DISCONNECT))
                        break;
                } catch (NumberFormatException e) {} catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }

        //listSong.stream().forEach(System.out::println);

    }
}
*/
