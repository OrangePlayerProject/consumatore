package org.orangeplayer.consumatore.ontesting;

import org.orangeplayer.common.beans.NetworkInfo;
import org.orangeplayer.common.response.Song;
import org.orangeplayer.consumatore.consumer.MusicConsumer;
import org.orangeplayer.consumatore.consumer.NetworkConsumer;
import org.orangeplayer.consumatore.consumer.SystemConsumer;
import org.orangeplayer.consumatore.net.Connector;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class TestConsuming {

    private static final Connector connector;
    private static final NetworkConsumer netCons;
    private static final SystemConsumer sysCons;
    private static final MusicConsumer musicCons;

    static {
        connector = new Connector("192.168.56.1");
        netCons = connector.getNetworkConsumer();
        sysCons = connector.getSystemConsumer();
        musicCons = connector.getMusicConsumer();
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        //login();
        //synchronize();
        //sysUtils();
        menu();
        disconnect();
    }

    public static void disconnect() throws IOException {
        System.out.println("Disconnect: "+netCons.disconnect());
    }

    public static void login() throws IOException {
        System.out.println("Login");
        System.out.println("---------------");
        System.out.println("IsConnected: "+netCons.isConnected());
        System.out.println("Connect: "+netCons.connect());
        System.out.println("---------------");
    }

    public static void synchronize() throws IOException {
        System.out.println("Synchronize");
        System.out.println("-----------");
        ArrayList<NetworkInfo> networks = netCons.getNetworks();
        System.out.println("getNets: "+ networks);
        if (!networks.isEmpty())
            System.out.println("FirstNet: "+networks.get(0).getName());

        System.out.println("EthIp: "+netCons.getEthernetIp());
        System.out.println("WlanIp: "+netCons.getWlanIp());

        System.out.println("ConnectWifi: "+netCons.connectWifi("MARTIN ", "admin097"));
        System.out.println("-----------");
    }

    public static void sysUtils() throws IOException {
        System.out.println("SysUtils");
        System.out.println("------------");
        System.out.println("ListDevs: "+sysCons.getListDevices());
        System.out.println("ListLabels: "+sysCons.getDevicesLabel());

        final String DEV_NAME = "sda1";

        boolean isMounted = sysCons.isMounted(DEV_NAME);
        System.out.println("IsMounted: "+isMounted);
        if (isMounted) {
            System.out.println("MountPoint: "+sysCons.getMountPoint(DEV_NAME));
            System.out.println("Umount: "+sysCons.umountDevice(DEV_NAME));
        }
        else {
            isMounted = sysCons.mountDevice(DEV_NAME);
            System.out.println("NowMounted: "+isMounted);
            if (isMounted) {
                System.out.println("MountPoint: "+sysCons.getMountPoint(DEV_NAME));
                System.out.println("Umount: "+sysCons.umountDevice(DEV_NAME));
            }
        }
    }

    public static void menu() throws IOException, ClassNotFoundException {
        System.out.println("Menu");
        System.out.println("------------------");
        System.out.println("StartPlayer: "+musicCons.startPlayer());
        System.out.println("Mute: "+musicCons.mute());
        System.out.println("GetGain: "+musicCons.getGain());
        System.out.println("GetProgress: "+musicCons.getProgress());
        System.out.println("Next1: "+musicCons.next());
        System.out.println("Next2: "+musicCons.next());
        System.out.println("Prev: "+musicCons.prev());
        musicCons.stop();
        System.out.println("Stop");
        musicCons.resume();
        System.out.println("Resume");
        Song current = musicCons.getCurrentInfo();
        System.out.println("Current: "+ current);
        System.out.println("CurrentTitle: "+ current.getTitle());
        byte[] currentCover = musicCons.getCoverFromCurrent();
        System.out.println("CurrentCover: "+ Arrays.toString(currentCover==null?new byte[0]:currentCover));
        //System.out.println("ListSongs: "+musicCons.getListSongs());
        System.out.println("------------------");
    }

}
