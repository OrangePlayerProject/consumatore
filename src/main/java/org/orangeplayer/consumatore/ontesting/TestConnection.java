package org.orangeplayer.consumatore.ontesting;/*package org.orangeplayer.org.orangeplayer.consumatore.ontesting;

import org.orangeplayer.org.orangeplayer.consumatore.net.Connector;
import org.orangeplayer.common.response.Song;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.Scanner;

public class TestConnection {
    public static void main(String[] args) {
        try {
            Connector connector = new Connector();

            connector.connect();
            Scanner scanner = new Scanner(System.in);

            if (connector.isConnected()) {
                System.out.println("Connected to servizio");
                String line;
                char c;

                boolean playerStarted = connector.isPlayerStarted();
                System.out.println("PlayerStarted: " + playerStarted);

                if (playerStarted)
                    loadCurrentInfo(connector);

                while (true) {
                    try {
                        line = scanner.nextLine().trim();
                        c = line.charAt(0);

                        switch (c) {
                            case 'n':
                                connector.next();
                                loadCurrentInfo(connector.getCurrentInfo());

                                break;

                            case 'p':
                                connector.prev();
                                loadCurrentInfo(connector.getCurrentInfo());
                                break;

                            case 's':
                                System.out.println(connector.stop());
                                break;

                            case 'r':
                                System.out.println(connector.resume());
                                break;

                            case 'm':
                                System.out.println(connector.pause());
                                break;

                            case 'v':
                                System.out.println(connector.setGain(Float.parseFloat(line.split(" ")[1])));
                                break;

                            case 'k':
                                if (line.split(" ").length > 1) {
                                    connector.seekFld(Integer.parseInt(line.split(" ")[1]),
                                            Integer.parseInt(line.split(" ")[2]) == 1);
                                    loadCurrentInfo(connector.getCurrentInfo());
                                } else
                                    System.out.println(connector.seek(Integer.parseInt(line.split(" ")[1])));
                                break;

                            case 'c':
                                loadCurrentInfo(connector.getCurrentInfo());
                                break;

                            case 'h':
                                System.out.println(connector.shutdown());
                                break;

                            case 't':
                                System.out.println(playerStarted);
                                break;

                            case 'e':
                                if (!connector.isPlayerStarted()) {
                                    connector.startPlayer();
                                    //loadCurrentInfo(connector);
                                }
                                break;
                        }
                    } catch (Exception e) {
                        System.err.println("Exception: " + e.toString());
                    }
                }

            } else
                System.out.println("Server is not running");
        } catch (IOException e) {
            System.err.println("Server not available");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void loadCurrentInfo(Song currentInfo) throws IOException {
        if (currentInfo.hasCover()) {
            File coverFile = new File("/home/martin/Testings/cover.png");
            coverFile.createNewFile();
            Files.write(
                    coverFile.toPath(),
                    currentInfo.getCoverData(),
                    StandardOpenOption.TRUNCATE_EXISTING);
            System.out.println("File has cover");
            currentInfo.setCoverData(new byte[0]);
        } else
            System.out.println("File haven't cover");

        System.out.println(currentInfo);
    }

    private static void loadCurrentInfo(Connector connector) throws IOException, ClassNotFoundException {
        System.out.println("Loading current info.....");
        loadCurrentInfo(connector.getCurrentInfo());
    }

}
*/
